import React from "react";
import { createAppContainer } from "react-navigation";
import AppNavigator from "./screens/_routers";
import NavigatorService from "./util/navigator";


const AppContainer = createAppContainer(AppNavigator);

class App extends React.Component {
    render() {

        console.disableYellowBox = true;
        
        return (
            <AppContainer
                ref={navigatorRef => {
                    NavigatorService.setContainer(navigatorRef);
                }}
            />
        );
    }
}

export default App;
