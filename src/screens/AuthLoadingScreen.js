import React from 'react';
import {ActivityIndicator, AsyncStorage, StatusBar, StyleSheet, View} from 'react-native';
import BaseScreen from "./BaseScreen";


const style = StyleSheet.create({});


class AuthLoadingScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    _bootstrapAsync = async () => {
        // await AsyncStorage.clear();
        const userToken = await AsyncStorage.getItem('access_token');
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        this.props.navigation.navigate(userToken ? 'AuthApp' : 'UnAuthApp');
    };

    render() {
        return (
            <View>
                <ActivityIndicator/>
                <StatusBar barStyle="default"/>
            </View>
        );
    }
}


export default AuthLoadingScreen;
