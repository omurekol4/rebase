import React from "react";


class BaseScreen extends React.Component {
  static navigationOptions = {
    // headerTitle: props => <Header {...props} />,
    headerBackTitle: null,
    headerStyle: {
      backgroundColor: "#5e3fc0",
      elevation: 0,
      shadowOpacity: 0
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      // fontWeight: "bold",
      fontStyle: "normal",
      fontSize: 26,
    }
  };

  constructor(props) {
    super(props);
  }
}

export default BaseScreen;
