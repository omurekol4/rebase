import React from "react";
import BaseScreen from "./BaseScreen";
import { Container, Header, Content, Button, Text } from 'native-base';
import NavigatorService from "../util/navigator";

export default class HomeScreen extends BaseScreen {


  constructor(props) {
    super(props);
  }

  goToLoginScreen(){
    NavigatorService.navigate("Login");
  }

  render() {

    return (
      <Container>
        <Content>
          <Button dark onPress={this.goToLoginScreen.bind(this)}>
            <Text> Login </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}



