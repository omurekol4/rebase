import React from "react";
import BaseScreen from "./BaseScreen";
import { Container, Content, Form, Item, Input, Button, Text } from 'native-base';
import axios from 'axios';
import { AsyncStorage } from 'react-native';

import NavigatorService from "../util/navigator";


export default class LoginScreen extends BaseScreen {

  TOKEN_URL = ''
  LOGIN_URL = '';
  USER_TOKEN;

  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: ""
    };

    TOKEN_URL = 'http://10.0.2.2:8080/auth/realms/react/protocol/openid-connect/token';
    
    this.getToken();
  }

  transformRequest = (jsonData = {}) =>
    Object.entries(jsonData)
      .map(x => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`)
      .join('&');

 
  getToken() {

    const data = {
      client_id: 'mobile-app',
      client_secret: '7313f5bc-90be-4e1d-83cd-95ceaebdd489',
      username: 'apiuser',
      password: '12345678',
      grant_type: 'password'
    };

    debugger;
    axios.post(TOKEN_URL, this.transformRequest(data))
      .then(response => {
        console.log(response.data);
        USER_TOKEN = response.data.access_token;
        console.log('userresponse ' + response.data.access_token);
      })
      .catch((error) => {
        console.log('error ' + error);
      });
  }

  doLogin() {
    let { username, password } = this.state;

    const request = {
      client_id: 'mobile-app',
      client_secret: '7313f5bc-90be-4e1d-83cd-95ceaebdd489',
      username: username,
      password: password,
      grant_type: 'password'
    };

    debugger;
    const AuthStr = 'Bearer '.concat(USER_TOKEN);
    axios.post(TOKEN_URL, this.transformRequest(request), { headers: {  } }).then(response => {
      this._saveToken(response.data);
    })
      .catch((error) => {
        console.log('error 3 ' + error);
      });

  }

  _saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('access_token', token.access_token);
      await AsyncStorage.setItem('refresh_token', token.refresh_token);
      debugger;
      NavigatorService.navigate("AuthApp");
    } catch (error) {
      alert("Oppppsss");
    }
  }

  render() {

    return (
      <Container>
        <Content>
          <Form>
            <Item>
              <Input placeholder="Username"
                value={this.state.username}
                onChangeText={(text) => this.setState({ username: text })}
              />
            </Item>
            <Item >
              <Input placeholder="Password"
                value={this.state.password}
                onChangeText={(text) => this.setState({ password: text })}
              />
            </Item>
            <Item last>
              <Button onPress={this.doLogin.bind(this)} >
                <Text>Click Me!</Text>
              </Button>
            </Item>
          </Form>
        </Content>
      </Container>
    );
  }
}



