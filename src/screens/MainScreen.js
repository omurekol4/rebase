import React from "react";
import BaseScreen from "./BaseScreen";
import { Container, Header, Content, Button, Text, List, ListItem } from 'native-base';
import NavigatorService from "../util/navigator";
import { AsyncStorage } from 'react-native';
import axios from 'axios';

export default class MainScreen extends BaseScreen {
  USER_TOKEN =   "";
  constructor(props) {
    
    super(props);
    console.log("constructor");
    
     this.state = {
      user: {}
    };
    
    LOGOUT_URL = "http://10.0.2.2:8080/auth/realms/react/protocol/openid-connect/logout";
    AsyncStorage.clear;

  }
  transformRequest = (jsonData = {}) =>
    Object.entries(jsonData)
      .map(x => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`)
      .join('&');

  logout = async () => {
    
    const userToken = await AsyncStorage.getItem('access_token');
    const refreshToken = await AsyncStorage.getItem('refresh_token');
    const request = {
      client_id: 'mobile-app',
      client_secret: '7313f5bc-90be-4e1d-83cd-95ceaebdd489',
      username : 'apiuser',
      password : 12345678,
      grant_type: 'refresh_token',
      refresh_token : refreshToken,

    };
    debugger; 
    console.log("Logoout token : "+refreshToken);
    const AuthStr = 'Bearer '.concat(refreshToken);
     axios.post(LOGOUT_URL, this.transformRequest(request), { headers: {  } }).then(response => {
        AsyncStorage.clear;
      NavigatorService.navigate("UnAuthApp");
    })
      .catch((error) => {
        console.log('error 3 ' + error);
      });   
  };


  async parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = decodeURIComponent(atob(base64Url).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(base64);
  };


  async componentWillMount() {
    console.log("componentWillMount");
    const userToken = await AsyncStorage.getItem('access_token');
    let user = await this.parseJwt(userToken);
  
    this.setState({ 
      user:user 
    })
    
  }

  render() {
    console.log("render");

    let { user } = this.state;

    return (
      <Container>
        <Content>
          <Text> Login olmuştur muberek </Text>

          <Button onPress={() => this.logout()}>
            <Text>Logout</Text>
          </Button>

          <List>
            <ListItem itemDivider>
              <Text>User Detail</Text>
            </ListItem>

            <ListItem>
              <Text>{user.name}</Text>
            </ListItem>
            <ListItem>
              <Text>{user.email}</Text>
            </ListItem> 
          </List> 
        </Content>
      </Container>
    );
  }

  componentDidMount() {
    console.log("componentDidMount");
  }


  componentWillUnmount() {
    console.log("componentWillUnmount");
  }

  componentWillUnmount(){

  }


}



