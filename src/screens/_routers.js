
import React from "react";
import {createStackNavigator, createSwitchNavigator, createAppContainer} from "react-navigation";

import LoginScreen from "./LoginScreen";
import HomeScreen from "./HomeScreen";
import MainScreen from "./MainScreen";
import AuthLoadingScreen from "./AuthLoadingScreen";

const UnAuthNavigator = createStackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
  },
  {
    initialRouteName: "Login"
  }
);

const AuthNavigator = createStackNavigator(
  {
    Main: {
      screen: MainScreen
    }
  },
  {
    initialRouteName: "Main"
  }
);

const AppNavigator = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      UnAuthApp: UnAuthNavigator,
      AuthApp: AuthNavigator
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
);

export default AppNavigator;